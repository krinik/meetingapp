package com.CalendarApp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.CalendarApp.model.Meeting;
import com.CalendarApp.service.MeetingService;

@RestController
public class MeetingController {

	@Autowired
	private MeetingService meetingService;

	@GetMapping(value = "/get", headers = "Accept=application/json")
	public List<Meeting> getAllUser() {
		List<Meeting> meetings = meetingService.getMeetings();
		return meetings;
	}

	@GetMapping(path = "/{meetId}")
	private Optional<Meeting> getMeeting(@PathVariable("meetId") int meetId) {
		System.out.println("Searching for record " + meetId);
		Optional<Meeting> meeting = meetingService.findMeetingById(meetId);
		if (meeting == null)
			return null;
		return meeting;

	}

	@PostMapping(path = "/createMeeting", headers = "Accept=application/json")
	private ResponseEntity<Void> setupMeeting(@RequestBody Meeting meeting, UriComponentsBuilder uric) {
		System.out.println("Creating meeting " + meeting);
		meetingService.setupMeeting(meeting);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uric.path("/meeting/{id}").buildAndExpand(meeting.getMeetUser()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
}
