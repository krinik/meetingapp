package com.CalendarApp.service;

import java.util.List;
import java.util.Optional;

import com.CalendarApp.model.Meeting;

public interface MeetingService {
	public void setupMeeting(Meeting meet);

	public Optional<Meeting> findMeetingById(int meetId);
	
	public List<Meeting> getMeetings();
}
