package com.CalendarApp.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "meeting")
public class Meeting {
	@Id
	private long meetId;

	@Indexed(unique = true)
	private String meetUser;
//	private Date meetDate = new Date();

//	private User users;

	public Meeting(long meetId, String meetUser) {//, Date meetDate) {
		super();
		this.meetId = meetId;
		this.meetUser = meetUser;
//		this.meetDate = meetDate;
	}

	public long getMeetId() {
		return meetId;
	}

	public void setMeetId(long meetId) {
		this.meetId = meetId;
	}

	public String getMeetUser() {
		return meetUser;
	}

	public void setMeetUser(String meetUser) {
		this.meetUser = meetUser;
	}

//	public Date getMeetDate() {
//		return meetDate;
//	}
//
//	public void setMeetDate(Date meetDate) {
//		this.meetDate = meetDate;
//	}

//	public User getUsers() {
//		return users;
//	}
//
//	public void setUsers(User users) {
//		this.users = users;
//	}

}
