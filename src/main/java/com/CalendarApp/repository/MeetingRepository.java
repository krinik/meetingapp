package com.CalendarApp.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.CalendarApp.model.Meeting;

@Repository
public interface MeetingRepository extends MongoRepository<Meeting, Integer> {

}
